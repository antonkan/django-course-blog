# Django Course - Blog

## About

This project is used as a template for further django projects to look up basic functionality.

The result can be visited here: 

## Useful commands
- Collect static files
```
python manage.py collectstatic
```
- Run local dev server
```
python manage.py runserver  
```
- Open shell
```
python manage.py shell 
```
- Create migration based on current changes
```
python manage.py makemigrations
```
- Run all new migrations
```
python manage.py migrate
```
- Create admin user
```
python manage.py createsuperuser
```

## Create virtual environment and requirements.txt for hoster
1. Create virtual environment for current project
    ```
    python -m venv <VENV_NAME>
    // here:
    python -m venv django_my_site
    ```
1. Open new terminal tab afterwards to activate
1. Install packages in current environment
    ```
    python -m pip install <PACKAGE1> <PACKAGE2> ...
    // here:

    python -m pip install Django Pillow
    ```
1. Create requirements.txt file for web hoster
    ```
    python -m pip freeze > requirements.txt
    ```

## Deployment
1. Create all static files
    ```
    python manage.py collectstatic
    ```
1. Zip file selection of project

    ![File selection](readme/res/deploy_file_select.jpg "File selection")