from django.http import Http404, HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

# Create your views here.

dict = {
        "january": "Eat no meat for the entire month!",
        "february": "Eat no sugar for the entire month!",
        "march": "Drink no alcohol for the entire month!",
        "april": "Do some studying every day!",
        "may": "Read for at least 10 minutes every day!",
        "june": "Walk for at least 20 minutes every day!",
        "july": "Do at least 1 unit of sports every day!",
        "august": None,
        "september": "Do some DJing for at least 5 minutes every day!",
        "october": "Do not buy anything that is not necessary (except food)",
        "november": "Do not smoke for the entire month!",
        "december": "Meditate every day for at least 2 minutes!"
    }

def months_overview(request):
    return render(request, "challenges/index.html", {
        "months": list(dict.keys())
    })


def monthly_challenge_int(request, month):
    months = list(dict.keys())
    if month > 0 and month < len(months) + 1:
        redirect_month = months[month - 1]
        redirect_path = reverse("month-challenge", args=[redirect_month])
        return HttpResponseRedirect(redirect_path)
    raise Http404()


def monthly_challenge(request, month):
    if month in dict:
        return render(request, "challenges/challenge.html", {
            "month": month,
            "text": dict[month],
        })
    raise Http404()
