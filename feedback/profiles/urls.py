from django.conf import settings
from django.urls import path

from . import views

urlpatterns = [
    path("", views.CreateProfileView.as_view()),
    path("all/", views.ProfilesView.as_view()),
]
