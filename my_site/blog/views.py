from datetime import date
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views.generic.list import ListView
from django.views import View

from .models import Post
from .forms import CommentForm


class StartingPageView(ListView):
    template_name = "blog/index.html"
    model = Post
    ordering = ["-date"]
    context_object_name = "posts"

    def get_queryset(self):
        return super().get_queryset()[:3]


class PostsView(ListView):
    template_name = "blog/all-posts.html"
    model = Post
    ordering = ["-date"]
    context_object_name = "all_posts"


class PostDetailView(View):
    def is_stored_post(self, request, post_id):
        stored_posts = request.session.get("stored_posts")
        if stored_posts is not None:
            return post_id in stored_posts
        else:
            return False
        


    def get(self, request, slug):
        post = Post.objects.get(slug=slug)
        
        return render(request, "blog/post-detail.html", {
            "post": post,
            "post_tags": post.tags.all(),
            "comments": post.comments.all().order_by("-id"),
            "comment_form": CommentForm(),
            "is_saved": self.is_stored_post(request, post.id),
        })

    def post(self, request, slug):
        post = Post.objects.get(slug=slug)
        comment_form = CommentForm(request.POST)

        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.post = post
            comment.save()
            return HttpResponseRedirect(reverse("post-detail-page", args=[slug]))
        else:
            return render(request, "blog/post-detail.html", {
                "post": post,
                "post_tags": post.tags.all(),
                "comments": post.comments.all().order_by("-id"),
                "comment_form": comment_form,
                "is_saved": self.is_stored_post(request, post.id),
            })


class ReadLaterView(View):
    def get(self, request):
        stored_posts = request.session.get("stored_posts")

        if stored_posts is None or len(stored_posts) == 0:
            posts = []
        else:
            posts = Post.objects.filter(id__in=stored_posts)

        context = {
            "posts": posts,
            "has_posts": len(posts) > 0,
        }

        return render(request, "blog/stored-posts.html", context)

    def post(self, request):
        stored_posts = request.session.get("stored_posts")

        if stored_posts is None:
            stored_posts = []

        post_id = int(request.POST["post_id"])

        if post_id not in stored_posts:
            stored_posts.append(post_id)
        else:
            stored_posts.remove(post_id)

        request.session["stored_posts"] = stored_posts

        return HttpResponseRedirect("/blog/")