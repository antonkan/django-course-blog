from django.db import models

# Create your models here.


class Author(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email_address = models.EmailField()

    def __str__(self):
        return f"{self.last_name}, {self.first_name}"


class Tag(models.Model):
    caption = models.CharField(max_length=255)

    def __str__(self):
        return self.caption


class Post(models.Model):
    title = models.CharField(max_length=255)
    excerpt = models.TextField()
    image = models.ImageField(upload_to="posts", null=True)
    date = models.DateTimeField(auto_now=True)
    slug = models.SlugField(blank=False, null=False, db_index=True)
    content = models.TextField()
    author = models.ForeignKey(
        Author, null=True, on_delete=models.SET_NULL, related_name="posts")
    tags = models.ManyToManyField(Tag, related_name="posts")

    def __str__(self) -> str:
        return f"{self.slug}"
    

class Comment(models.Model):
    user_name = models.CharField(max_length=120)
    user_email = models.EmailField()
    text = models.TextField(max_length=400)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="comments")
    
